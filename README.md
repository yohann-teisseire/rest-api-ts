# REST API IN TS

DATABASE
------
The database is `Postgres`.
1. Execute the `sql/data.sql` in `pgAdmin`

PROJECT
------
1. Download the project
2. Execute `npm update`

DATABASE ORM
-----
Sequelize is a promise-based ORM for Node.js v4 and up. It supports the dialects PostgreSQL, MySQL, SQLite and MSSQL and features solid transaction support, relations, read replication and more.
